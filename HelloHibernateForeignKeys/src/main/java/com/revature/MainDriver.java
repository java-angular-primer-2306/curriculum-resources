package com.revature;

import java.util.ArrayList;
import java.util.List;

import com.revature.config.HibernateConfig;
import com.revature.model.Feature;
import com.revature.model.Moon;
import com.revature.model.Planet;
import com.revature.repo.FeatureDao;
import com.revature.repo.MoonDao;
import com.revature.repo.PlanetDao;

public class MainDriver {

	private static PlanetDao pDao = new PlanetDao();
	private static MoonDao mDao = new MoonDao();
	private static FeatureDao fDao = new FeatureDao();
	
	private static HibernateConfig hc = HibernateConfig.getHibernateConfig();
	
	public static void main(String[] args) {
	
//		HibernateConfig.configuringSessionFactory(); // configure everything first. 
		
		
		
		initializeEverything();
		
//		System.out.println(pDao.getPlanetByName("Earth"));
//		System.out.println(pDao.getPlanetByName("Mars"));
		
		
		List<Planet> p = pDao.getPlanetByName("Earth");
		
		HibernateConfig.sf.close();
		
		System.out.println(p.get(0).getName());
		System.out.println(p.get(0).getId());
		System.out.println(p.get(0).getMyMoons());
		System.out.println(p.get(0).getPlanetFeatures());

	}
	
	public static void initializeEverything() {
		Planet p1 = new Planet();
		Moon m1 = new Moon();
		Moon m2 = new Moon();
		List<Moon> moonList = new ArrayList<>();
		
		Feature f1 = new Feature();
		
		Feature f2 = new Feature();
		List<Planet> featuresPlanetList = new ArrayList<>();
		
		
		f1.setPlanetList(featuresPlanetList);
		List<Feature> featureList = new ArrayList<>();
		
		
		Planet p2 = new Planet();
		Moon p2Moon = new Moon();
		p2Moon.setMyPlanet(p2);
		p2.setName("Mars");
		p2Moon.setName("Deimos");
		
		List<Moon> moonListP2 = new ArrayList<>();
		moonListP2.add(p2Moon);
		p2.setMyMoons(moonListP2);
		p2.setMyMoons(new ArrayList<>());
		
		
		
		
		
		f1.setName("Mountains");
		f2.setName("Atmosphere");
//		f1.setName("Rings");
		featureList.add(f2);
		featureList.add(f1);
		
		
		m1.setName("The Moon");
		m1.setMyPlanet(p1);
		
		m2.setName("Fake Moon");
		m2.setMyPlanet(p1);
		
		moonList.add(m1);
		moonList.add(m2);
		
		
		p1.setName("Earth");
		p1.setMyMoons(moonList);
		p1.setPlanetFeatures(featureList);
		
		p2.setPlanetFeatures(featureList);
		
		
		featuresPlanetList.add(p1);
		featuresPlanetList.add(p2);
		f1.setPlanetList(featuresPlanetList);
		f2.setPlanetList(featuresPlanetList);
		
		pDao.insert(p1);
		pDao.insert(p2);
		mDao.insert(m1);
		mDao.insert(m2);
		mDao.insert(p2Moon);
		fDao.insert(f1);
		fDao.insert(f2);
	}

}
