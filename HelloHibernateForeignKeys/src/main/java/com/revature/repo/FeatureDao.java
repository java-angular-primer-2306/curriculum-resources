package com.revature.repo;



import org.hibernate.Session;
import org.hibernate.Transaction;

import com.revature.config.HibernateConfig;
import com.revature.model.Feature;

public class FeatureDao {
	
	public void insert(Feature f) {
		Session ses = HibernateConfig.getSession();
		Transaction tx = ses.beginTransaction();
		
		ses.save(f);
		
		tx.commit();
		ses.close();
	}

}
