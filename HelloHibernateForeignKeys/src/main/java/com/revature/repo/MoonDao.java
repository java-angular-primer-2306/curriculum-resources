package com.revature.repo;



import org.hibernate.Session;
import org.hibernate.Transaction;

import com.revature.config.HibernateConfig;
import com.revature.model.Moon;

public class MoonDao {
	
	public void insert(Moon m) {
		Session ses = HibernateConfig.getSession();
		Transaction tx = ses.beginTransaction();
		
		ses.save(m);
		
		tx.commit();
		ses.close();
	}

}
