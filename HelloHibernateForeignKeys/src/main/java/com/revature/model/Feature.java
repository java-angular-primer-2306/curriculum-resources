package com.revature.model;

import java.util.List;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "feature_table")
public class Feature {
	
	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column
	private String name;
	
	@ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<Planet> planetList;

	public Feature() {
		super();
		// TODO Auto-generated constructor stub
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Planet> getPlanetList() {
		return planetList;
	}

	public void setPlanetList(List<Planet> planetList) {
		this.planetList = planetList;
	}

	public Feature(int id, String name, List<Planet> planetList) {
		super();
		this.id = id;
		this.name = name;
		this.planetList = planetList;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, name, planetList);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Feature other = (Feature) obj;
		return id == other.id && Objects.equals(name, other.name) && Objects.equals(planetList, other.planetList);
	}

	@Override
	public String toString() {
		return "Feature [id=" + id + ", name=" + name + ", FirstPlanet=" + "placeholder" + "]";
	}

	
	
	

}
