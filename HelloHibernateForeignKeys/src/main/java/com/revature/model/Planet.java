package com.revature.model;

import java.util.List;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * 
 * JPA Annotation (Javax/Jarkata Persistence API) 
 * 
 * Is a standardized api that deals with mapping Java objects to records in tables. 
 * It is not a library or app, instead it defines a set of rules for all ORM (Object relatational mapping) libraries to follow. 
 * 
 * We choose JPA over Hibernate annotations because it makes it more modular. 
 *
 */

@Entity
@Table(name = "planet_table")
public class Planet {
	
	@Id
	@Column(name = "planet_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY) //will override the id you provide and generate a value
	private int id;
	
	@Column(name="planet_name", nullable=false)
	private String name;
	
	@OneToMany(mappedBy = "myPlanet", fetch = FetchType.LAZY)
	private List<Moon> myMoons;

	@ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private List<Feature> planetFeatures;

	public Planet() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Planet(int id, String name, List<Moon> myMoons, List<Feature> planetFeatures) {
		super();
		this.id = id;
		this.name = name;
		this.myMoons = myMoons;
		this.planetFeatures = planetFeatures;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Moon> getMyMoons() {
		return myMoons;
	}

	public void setMyMoons(List<Moon> myMoons) {
		this.myMoons = myMoons;
	}

	public List<Feature> getPlanetFeatures() {
		return planetFeatures;
	}

	public void setPlanetFeatures(List<Feature> planetFeatures) {
		this.planetFeatures = planetFeatures;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, myMoons, name, planetFeatures);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Planet other = (Planet) obj;
		return id == other.id && Objects.equals(myMoons, other.myMoons) && Objects.equals(name, other.name)
				&& Objects.equals(planetFeatures, other.planetFeatures);
	}

	@Override
	public String toString() {
		return "Planet [id=" + id + ", name=" + name + ", myMoons=" + myMoons + ", planetFeatures=" + planetFeatures
				+ "]";
	}
	
	
	
	
	
	
	

}
