package com.revature.repo;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import com.revature.config.HibernateConfig;
import com.revature.model.Planet;

public class PlanetDao {

	public void insert(Planet p) {
		
		Session ses = HibernateConfig.getSession();
		Transaction tx = ses.beginTransaction();
		
		ses.save(p);
		
		tx.commit();
		ses.close();
		
	}
	
	public void update(Planet p) {
		
		Session ses = HibernateConfig.getSession();
		Transaction tx = ses.beginTransaction();
		
		ses.update(p);
		
		tx.commit();
		ses.close();
		
	}
	
	public List<Planet> getAllPlanets(){
		return null;
	}
	
	public Planet getPlanetById(int id) {return null;};
	
	
	public List<Planet> getPlanetByName(String name) {
		
		Session ses = HibernateConfig.getSession();
		
		List<Planet> p = null;
		
		//Native SQL 
		
		p = ses.createNativeQuery("select * from planet_table where planet_name = '" + name + "'", Planet.class).list();
		
		//Hibernate Query Language 
		
		p = ses.createQuery("from Planet where name = '" + name + "'",Planet.class).list();
		
		//Criteria API (deprecated but Criteria builder is the new way)
		p = ses.createCriteria(Planet.class).add(Restrictions.ilike("name", name)).list();
		
		return p;
		
	}
	
	public Planet deletePlanet(Planet p) {
		return null;
	}
	
	
	
	
}
