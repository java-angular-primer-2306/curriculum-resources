package com.revature;

import com.revature.config.HibernateConfig;
import com.revature.model.Planet;
import com.revature.repo.PlanetDao;

public class MainDriver {

	private static PlanetDao pDao = new PlanetDao();
	
	private static HibernateConfig hc = HibernateConfig.getHibernateConfig();
	
	public static void main(String[] args) {
	
//		HibernateConfig.configuringSessionFactory(); // configure everything first. 
		
		
		Planet p1 = new Planet(0,"Earth");
		Planet p2 = new Planet(0,"Venus");
		Planet p3 = new Planet(0,"Mars");
		
		pDao.insert(p1);
		pDao.insert(p2);
		pDao.insert(p3);
		
		System.out.println(pDao.getPlanetByName("Earth"));
		System.out.println(pDao.getPlanetByName("Mars"));
		System.out.println(pDao.getPlanetByName("Venus"));

	}

}
