package com.revature.config;

import java.util.HashMap;
import java.util.Map;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import com.revature.model.Planet;

public class HibernateConfig {
	
	private static SessionFactory sf;
	private static HibernateConfig hc;
	
	private static void configuringSessionFactory() {
		
		
		Map<String,String> settings = new HashMap<>();
		
		settings.put("hibernate.connection.url", "jdbc:h2:mem:planetdb");
		settings.put("hibernate.connection.username","sun");
		settings.put("hibernate.connection.password", "password");
		
		settings.put("hibernate.connection.driver_class", "org.h2.Driver");
		settings.put("hibernate.connection.dialect", "org.hibernate.dialect.H2Dialect");
		settings.put("hibernate.hbm2ddl.auto", "create");
		settings.put("hibernate.show_sql", "false");
		settings.put("hibernate.format_sql", "true");
		
		StandardServiceRegistry standardRegistry = new StandardServiceRegistryBuilder()
				.applySettings(settings)
				.build();
		
		Metadata metadata = new MetadataSources(standardRegistry)
				.addAnnotatedClass(Planet.class)
				.getMetadataBuilder()
				.build();
		
		sf = metadata.getSessionFactoryBuilder().build();
		
		
	}
	
	private HibernateConfig() {
		configuringSessionFactory();
	}
	
	public static HibernateConfig getHibernateConfig() {
		if(hc == null) {
			hc = new HibernateConfig();
		}
		return hc;
	}
	
	public static Session getSession() {
		return sf.openSession();
	}

}
