package com.revature.model;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 * JPA Annotation (Javax/Jarkata Persistence API) 
 * 
 * Is a standardized api that deals with mapping Java objects to records in tables. 
 * It is not a library or app, instead it defines a set of rules for all ORM (Object relatational mapping) libraries to follow. 
 * 
 * We choose JPA over Hibernate annotations because it makes it more modular. 
 *
 */

@Entity
@Table(name = "planet_table")
public class Planet {
	
	@Id
	@Column(name = "planet_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY) //will override the id you provide and generate a value
	private int id;
	
	@Column(name="planet_name", nullable=false)
	private String name;
	
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Override
	public String toString() {
		return "Planet [id=" + id + ", name=" + name + "]";
	}
	@Override
	public int hashCode() {
		return Objects.hash(id, name);
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Planet other = (Planet) obj;
		return id == other.id && Objects.equals(name, other.name);
	}
	public Planet(int id, String name) {
		super();
		this.id = id;
		this.name = name;
	}
	public Planet() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	

}
