# Curriculum-Resources

This repository will contain notes and demos from lectures. Below are the topics that will be covered during the primer, with links to additional reading material.  


## Java Topics 
- [Java 11 features](https://www.baeldung.com/java-11-new-features)
- [Java 17 features](https://www.baeldung.com/java-17-new-features)

## Spring & Hibernate Topics 
- [Spring Boot](https://spring.io/projects/spring-boot) 
- [Spring Data](https://spring.io/projects/spring-data) 
- [Hibernate](https://hibernate.org/orm/documentation/6.2/) 
- [Java Persistance API annotations:](https://docs.oracle.com/javaee/6/tutorial/doc/bnbpz.html)
    - @Entity, @Table, @Id, @Column
    - @OneToOne, @OneToMany, @ManyToMany, @JoinColumn
- Distributed Transactions and XA
- Transaction Management

## [Angular](https://angular.io/docs)
- Angular-CLI
- Directives:
  - Components
  - Templates
- Services
- Pipes
- Modules
- Data binding:
  - Event
  - Interpolation
  - Property binding
  - Two-Way data binding

## [DevOps](https://www.atlassian.com/devops)
  - Software Development Lifecycles:
    - Iterative vs Waterfall vs Agile
    - Agile/Scrum
    - User stories, burndown charts
    - Iterations, retrospectives
    - Standup meetings
  - [Git SCM for teams:](https://nvie.com/posts/a-successful-git-branching-model/)
    - master, dev, feature branching
    - code reviews & pull requests
  - [Pipelines](https://www.atlassian.com/continuous-delivery/principles/continuous-integration-vs-delivery-vs-deployment)
    - Continuous Integration (CI):
      - source -> SCM -> build server
    - Continuous Delivery (CD)
      - build server -> :
        - QA/Test/UAT servers
        - Quality gate (SonarQube)
        - Code coverage
        - Staging/Production server
      - bash scripting

## Testing Resources
- Software Testing General
    - Software Testing Life Cycle, from Guru 99: [STLC](https://www.guru99.com/software-testing-life-cycle.html)
    - Guru 99 on the [Importance of Software Testing](https://www.guru99.com/software-testing-introduction-importance.html)
    - Guru 99 on [How to Write Test Cases in Software Testing](https://www.guru99.com/test-case.html)  
    - GeeksForGeeks: [Software Testing - Test Case](https://www.geeksforgeeks.org/software-testing-test-case/)
    - Atlassian's [Types of Software Testing](https://www.atlassian.com/continuous-delivery/software-testing/types-of-software-testing)
    - [Levels of Testing](https://www.javatpoint.com/levels-of-testing) from javaTpoint.com
- Testing Tools
    - [JUnit 5](https://junit.org/junit5/docs/current/user-guide/)
    - [Mockito](https://site.mockito.org/)
    - [Jasmine](https://jasmine.github.io/)
    - [Karma](https://karma-runner.github.io/latest/index.html)
    - [Cypress](https://www.cypress.io/)
- Testing Spring Applications
    - Unit (Component) Testing
        - JUnit & Mockito
    - Integration Testing 
        - Data JPA Testing
        - MockMVC
        - RestTemplate
        - [Spring's Guide to Testing the Web Layer](https://spring.io/guides/gs/testing-web/)
- Testing Angular Applications
    - Unit (Component) Testing
    - Integration Testing
    - Angular TestBed
    - [Angular's Guides to Testing](https://angular.io/guide/testing)
- E2E Testing
    - Cypress
    - [Testing Angular Applications with Cypress](https://testing-angular.com/end-to-end-testing/)