package com.revature.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.revature.models.Planet;

@RestController
@CrossOrigin(origins = "*")
public class PlanetController {
	
	private List<Planet> planetList = new ArrayList<>();

	
	@GetMapping("/planets")
	public List<Planet> getPlanets(){
		
		Planet p1 = new Planet("Earth","Fake URL",5);
		this.planetList.add(p1);
		
		return this.planetList;
		
	}
	
}
