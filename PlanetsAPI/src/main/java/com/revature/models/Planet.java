package com.revature.models;

import java.util.Objects;

public class Planet {
	
	
	private String name;
	private String image;
	private int livibility;
	
	@Override
	public int hashCode() {
		return Objects.hash(image, livibility, name);
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Planet other = (Planet) obj;
		return Objects.equals(image, other.image) && livibility == other.livibility && Objects.equals(name, other.name);
	}
	@Override
	public String toString() {
		return "Planet [name=" + name + ", image=" + image + ", livibility=" + livibility + "]";
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public int getLivibility() {
		return livibility;
	}
	public void setLivibility(int livibility) {
		this.livibility = livibility;
	}
	public Planet() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Planet(String name, String image, int livibility) {
		super();
		this.name = name;
		this.image = image;
		this.livibility = livibility;
	}
	
}
