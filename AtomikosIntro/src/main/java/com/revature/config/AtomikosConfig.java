package com.revature.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.jta.JtaTransactionManager;

import com.atomikos.icatch.jta.UserTransactionManager;

import jakarta.transaction.TransactionManager;
import jakarta.transaction.UserTransaction;


@Configuration 
@EnableTransactionManagement
public class AtomikosConfig {
	
	@Bean
	public UserTransactionManager userTransactionManager() {
		UserTransactionManager utM = new UserTransactionManager();
		return utM;
	}
	
	@Bean
	public JtaTransactionManager transactionManager() {
		
		JtaTransactionManager jtaTransactionManager = new JtaTransactionManager();
		
		UserTransactionManager utM = userTransactionManager();
		
		jtaTransactionManager.setTransactionManager((TransactionManager) utM);
		jtaTransactionManager.setUserTransaction((UserTransaction) utM);
		
		return jtaTransactionManager;
		
	}
	
//	@Bean(name="transactionManager")
//	@ConfigurationProperties("spring.jta")
//	public PlatformTransactionManager transactionManager() {
//		
//		
//		return new JtaTransactionManager();
//	}

}
