package com.revature.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import jakarta.persistence.EntityManagerFactory;

@Configuration
@EnableJpaRepositories(
		entityManagerFactoryRef="planetEntityManagerFactory",
		transactionManagerRef= "transactionManager",
		basePackages= "com.revature.repo"
		)
public class PlanetDataConfig {

	
	@Bean(name="planetProperties")
	@ConfigurationProperties("spring.jta.atomikos.datasource.one")
	public DataSourceProperties dataSourceProperties() {
		
		return new DataSourceProperties();
	}
	
	@Bean(name="planetDataSource")
	public DataSource dataSource(@Qualifier("planetProperties") DataSourceProperties properties) {
		return properties.initializeDataSourceBuilder().build();
	}
	
	//Entity Manager - resposible for managing our entities i.e. Planet.class
	@Bean(name = "planetEntityManagerFactory")
	public LocalContainerEntityManagerFactoryBean entityManagerFactoryBean(
			EntityManagerFactoryBuilder builder,
			@Qualifier("planetDataSource")DataSource dataSource
			) {
		
		return builder.dataSource(dataSource)
				.packages("com.revature.models")
				.persistenceUnit("planet").build();
		
	}
	
	
	
}
