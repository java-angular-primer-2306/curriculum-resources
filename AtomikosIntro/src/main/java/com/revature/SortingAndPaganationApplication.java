package com.revature;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 
 * Distributed Transactions and XA
 * 
 * Distributed Transactions are used in a distributed system (where you can have multiple sources of data, REST API, 
 * 	SQL, NoSQL, SMTP, etc .). Whenever a transaction needs to be performed, multiple parties can be involved and all these
 * 	parties have to make sure they can complete the operation. 
 * 
 * 
 * XA protocol (eXtensible Architecture), is a specifiction developed by the Open Group. It defines a two-phase commit 
 * 	protocol (2PC). 2PC is used to make sure that transactions are valid in a distributed systems, even in the presence 
 * 	of failure. 
 * 
 * 2PC protocol: 
 * 
 * Entities involved in this are: Transaction Coordinator and Transaction Participants 
 * 
 *  Coordinator: responsible for everything, communicates with all the participants in the transaction
 *  Participants: every resource or database involved with the transaction. 
 *  
 *  1. Phase 1: coordinator sends a prepare request to all participants. Asking them to commit. 
 *  	Each participant will see if they can or if an error occurs. 
 *  
 *  2. Voting: All participants vote to see if they can complete the operation. If not, then the transaction is cancelled. 
 *  3. Phase 2: Coordinator sends another request to commit all the operations, to each participant. 
 * 	
 * 
 * What kinds of distributed transaction managers can we use?
 * 	Narayana 
 * 	Bitronix
 * 	Atomikos 
 *
 */


@SpringBootApplication
public class SortingAndPaganationApplication {

	public static void main(String[] args) {
		SpringApplication.run(SortingAndPaganationApplication.class, args);
	}

}
