package com.revature.repo;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;

import com.revature.models.Planet;

public interface PlanetRepo extends JpaRepository<Planet, Integer>{
	
	
	
	
	Planet findByNameIgnoreCase(String name);
	
	List<Planet> findAllByOrderByMassAsc();

	List<Planet> findAllByOrderByMassDesc();
	
	List<Planet> findAll(Sort sort);
	
	Page<Planet> findAll(Pageable pageable);
	
	List<Planet> findByMass(Double mass, Pageable pageable); //Finding mass and paging it
	
	List<Planet> findByMassBetween(double massStart, double massEnd);
	
	
	/*
	 * Custom Querys that we would like: 
	 * 
	 * 		Retrieve all planets in order of mass
	 * 
	 * 		Retrieve all planets in descending order of name
	 * 
	 * 		Retrieve 3 planets at a time, that have identical masses. 
	 * 
	 * 		Retrieve planets within a certain mass range. 
	 * 
	 * 	
	 * 		TODO:
	 * 			Retrieve all planets that have rings 
	 * 
	 * 			Order by Mass (from biggest to smallest) all planets that have rings 
	 * 
	 * 			Retrieve in batches of 3, planets between certain masses that have rings. 
	 * 		
	 */

}
