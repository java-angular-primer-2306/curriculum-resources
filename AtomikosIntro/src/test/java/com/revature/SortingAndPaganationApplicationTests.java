package com.revature;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import com.revature.models.Planet;
import com.revature.repo.PlanetRepo;

@SpringBootTest
class SortingAndPaganationApplicationTests {
	
	@Autowired
	PlanetRepo planetRepo;
	
	void initalizeValues() {
		Planet p1 = new Planet();
		p1.setId(1);
		p1.setName("Mercury");
		p1.setMass(0.1);
		p1.setHasRings(true);
		
		Planet p2 = new Planet();
		p2.setId(2);
		p2.setName("Earth");
		p2.setMass(1);
		p2.setHasRings(true);
		
		Planet p3 = new Planet();
		p3.setId(3);
		p3.setName("Venus");
		p3.setMass(0.1);
		p3.setHasRings(false);
		
		Planet p11 = new Planet();
		p11.setId(4);
		p11.setName("Mercury1");
		p11.setMass(1.1);
		p11.setHasRings(false);
		
		Planet p22 = new Planet();
		p22.setId(5);
		p22.setName("Earth2");
		p22.setMass(1.1);
		p22.setHasRings(true);
		
		Planet p33 = new Planet();
		p33.setId(6);
		p33.setName("Venus3");
		p33.setMass(1.1);
		p22.setHasRings(true);
		
		planetRepo.save(p1);
		planetRepo.save(p2);
		planetRepo.save(p3);
		planetRepo.save(p11);
		planetRepo.save(p22);
		planetRepo.save(p33);
		
	}

	@Test
	void testSave() {
		
		Planet p = new Planet();
		p.setId(1);
		p.setName("Mercury");
		p.setMass(0.1);
		
		Planet savedPlanet = planetRepo.save(p);
		
		Planet retrievedPlanet = planetRepo.findById(savedPlanet.getId()).get();
		
		assertNotNull(retrievedPlanet);
	}
	
	@Test
	void testRetrieve() {
		
		initalizeValues();
		
//		System.out.println(planetRepo.findByNameIgnoreCase("earth"));
//		
//		System.out.println(planetRepo.findAll());
//		
//		System.out.println(planetRepo.findAllByOrderByMassAsc());
//		
//		System.out.println(planetRepo.findAllByOrderByMassDesc());
//		
//		System.out.println(planetRepo.findAll(Sort.by("name").descending()));
//		
//		Pageable firstTwo = PageRequest.of(0, 2); //This will give me the first 2 results.
//		
//		Pageable anotherPage = PageRequest.of(1, 2,Sort.by("mass").descending()); //This will give me the 2nd and 3rd heaviest result
//		
//		System.out.println(planetRepo.findAll(firstTwo).getContent());
//		
//		System.out.println(planetRepo.findAll(anotherPage).getContent());
		
		System.out.println(planetRepo.findByMassBetween(1, 2));
		
		System.out.println(planetRepo.findByMass(1.1, PageRequest.of(0, 3)));
		
	}

}
