package com.revature.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import jakarta.persistence.EntityManagerFactory;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
		entityManagerFactoryRef="planetEntityManagerFactory",
		transactionManagerRef= "planetTransactionManager",
		basePackages= "com.revature.repo"
		
		)
public class PlanetDataConfig {

	
	@Bean(name="planetProperties")
	@ConfigurationProperties("spring.datasource")
	public DataSourceProperties dataSourceProperties() {
		
		/*
		 * If we do entirely programmaticlly 
		 * 
		 * 
		 */
		
//		DataSourceProperties dp = new DataSourceProperties();
//		dp.setUsername(null);
//		dp.setUrl(null);
//		dp.setPassword(null);
//		dp.setDriverClassName(null);
		
		
		return new DataSourceProperties();
	}
	
	@Bean(name="planetDataSource")
	public DataSource dataSource(@Qualifier("planetProperties") DataSourceProperties properties) {
		return properties.initializeDataSourceBuilder().build();
	}
	
	//Entity Manager - resposible for managing our entities i.e. Planet.class
	@Bean(name = "planetEntityManagerFactory")
	public LocalContainerEntityManagerFactoryBean entityManagerFactoryBean(
			EntityManagerFactoryBuilder builder,
			@Qualifier("planetDataSource")DataSource dataSource
			) {
		
		return builder.dataSource(dataSource)
				.packages("com.revature.models")
				.persistenceUnit("planet").build();
		
	}
	
	//Transaction Manager is dependent on EntityManager and Data Source
	@Bean(name = "planetTransactionManager")
	@ConfigurationProperties("spring.jpa")
	public PlatformTransactionManager transactionManager(
			@Qualifier("planetEntityManagerFactory") EntityManagerFactory entityManagerFactory) {
		
		
		JpaTransactionManager jpaTM =  new JpaTransactionManager(entityManagerFactory);
//		jpaTM.set
		
		
		return jpaTM;
	}
	
	
}
