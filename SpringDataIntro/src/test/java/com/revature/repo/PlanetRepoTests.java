package com.revature.repo;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;
import java.util.NoSuchElementException;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.dao.DataIntegrityViolationException;

import com.revature.models.Planet;


// DataJpaTest is going to load only the persistence layer (anything annotated with @Repository/@Entity)
// it will ignore (and not load) @Component, @Service, @Controller...
// by default - DataJpaTest applies the @Transactional annotation - which rollsback any transactions
	// against the database
@DataJpaTest
public class PlanetRepoTests {

	
	@Autowired
	private PlanetRepo planetRepo;
	
	
	// not 100% necessary - we didn't create this method - it's from the framework
	@Test
	public void findAllPlanetsTest() {
		
		List<Planet> returnedPlanets = planetRepo.findAll();
		
		assertEquals(9, returnedPlanets.size());
		
	}
	
	@Test
	public void givenValidId_whenGetById_shouldReturnPlanet() {
		
		Planet returnedPlanet = planetRepo.findById(1).orElse(null);
		
		assertEquals(1, returnedPlanet.getId());
	}
	
	// this test is for a method that WE wrote - so we should test it
	@ParameterizedTest
	@ValueSource(
			strings = {"mercury", "venus", "earth", "mars", "jupiter", "saturn", "uranus", "neptune", "pluto"} )
	public void findPlanetByNameTest(String planetName) {
		Planet returnedPlanet = planetRepo.findByName(planetName);
		assertEquals(planetName, returnedPlanet.getName());
	}
	
	// test to make sure not null constraint is being observed
	@Test
	public void givenInvalidPlanet_whenSavePlanet_shouldNotSavePlanet() {
		
		Planet invalidPlanet = new Planet();
		assertThrows(DataIntegrityViolationException.class, () -> planetRepo.save(invalidPlanet));
	}
	
	@Test
	public void givenValidPlanet_whenSave_shouldReturnSavedPlanetDetails() {
		
		Planet planetToSave = new Planet();
		planetToSave.setName("ABGH8347163");
		
		Planet returnedPlanet = planetRepo.save(planetToSave);
		
		
		assertNotNull(returnedPlanet.getId());
		assertTrue(returnedPlanet.getId() > 0);
		
	}
	
	@DisplayName("should update resource")
	@Test
	public void updateTest() {
		Planet mockPlanet = new Planet();
		mockPlanet.setName("testing update");
		Planet savedPlanet = planetRepo.save(mockPlanet);
		
		savedPlanet.setName("updated name");
		planetRepo.save(savedPlanet);
		
		
		Planet retrievedPlanet = planetRepo.findById(savedPlanet.getId()).get();
		
		assertEquals("updated name", retrievedPlanet.getName());
		
	}
	@DisplayName("should delete resource")
	@Test
	public void deleteTest() {
		planetRepo.deleteById(1);
		
		assertThrows(NoSuchElementException.class, () -> planetRepo.findById(1).get());
	}
	
}
