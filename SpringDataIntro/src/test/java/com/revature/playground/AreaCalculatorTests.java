package com.revature.playground;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class AreaCalculatorTests {
	
	private AreaCalculator calculator;
	
	
	@BeforeEach
	public void setUp() {
		calculator = new AreaCalculator();
	}
	
	
	
	// this is what we call a Positive Test
	// we're testing the 'happy path'
	@DisplayName("The calculator can calculate the area of a square")
	@Test
	public void squareAreaTest() {
		// Arrange (Given Preconditions)
		double inputLength = 5.0;
		double expectedArea = 25.0;

		// Act (The execution of the method under test)
		
		double actualArea = calculator.squareAreaBasic(inputLength);
		
		// Assert (Validating that the actual outcome is what we expected)
		assertEquals(expectedArea, actualArea);
	}
	
	
	
	// Negative Test - we provide INvalid input to make sure our method
	// handles that appropriately. 
	@DisplayName("Given a negative double, should return 0")
	@Test
	public void squareAreaBasicNegativeTest() {
		
		double inputLength = -5;
		double expectedArea = 0;
		
		double actualArea = calculator.squareAreaBasic(inputLength);
		
		assertEquals(expectedArea, actualArea);
		
	}
	
	@DisplayName("Given a negative side length, should throw NegativeSideLengthException")
	@Test
	public void squareAreaExceptionTest() {
		
		double lengthInput = -5;
		// in junit5, we use assertThrows and pass the class of the exception we expect to be thrown and the Executable that should cause the error to be thrown 
		assertThrows(NegativeSideLengthException.class, () -> calculator.squareAreaException(lengthInput));
		
		
	}
	
	
}
