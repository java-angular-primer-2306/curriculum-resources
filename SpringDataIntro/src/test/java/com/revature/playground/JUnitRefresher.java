package com.revature.playground;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

public class JUnitRefresher {

	/*
	 * Unit Testing (Component Testing)
	 * - testing individual components of our code
	 * - low level testing
	 * - isolating our code as much as possible
	 * - this often means testing individual methods of our classes
	 */
	
	/*
	 * JUnit 5 Annotations (JUnit Jupiter)
	 * 
	 * @Test
	 * 
	 * @BeforeAll | JUnit 4 => @BeforeClass
	 * @BeforeEach | @Before
	 * @AfterAll
	 * @AfterEach
	 * 
	 * @Disabled (to ignore a test)
	 * 
	 * 
	 * Assertions
	 * 	- is a class of static helper methods that allow us to verify/validate 
	 * 		certain conditions
	 * 
	 */
	
	@BeforeAll
	public static void setUp() {
		System.out.println("This will ONCE run before all tests");
	}
	
	@Test
	public void test1() {
		System.out.println("This is a test 1!");
	
	}
	@Disabled
	@Test
	public void test2() {
		System.out.println("This is a test 2!");
	}
	@Test
	public void test3() {
		System.out.println("This is a test 3!");
	}
	
	@BeforeEach
	public void beforeEach() {
		System.out.println("This will print BEFORE each and every test");
	}
	
	@AfterEach
	public void afterEach() {
		System.out.println("This will print AFTER each and every test");
	}
	
	@AfterAll
	public static void teardown() {
		System.out.println("This will print ONCE AFTER all tests");
	}

}
