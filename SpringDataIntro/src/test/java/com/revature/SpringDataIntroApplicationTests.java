package com.revature;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.revature.models.Planet;
import com.revature.repo.PlanetRepo;


/*
 * Software Testing? 
 * 
 * - making sure our code does what it is supposed to do. 
 * - making sure the functionality we add in the future doesn't break existing functionality. 
 * 				- this is what we call regression testing. 
 * 				- comes into play with CI/CD
 * 				- Manual Testing / Automated Testing
 * - organizing and planning how we build a system to make sure we're building it right
 * 
 * - Quality Product 
 * 
 * 1. Does this system work the way it's supposed to.  
 * 
 * 2. Are we meeting the client's requirements.
 * 
 * 3. Did we make a quality product? 
 *
 * Software Testing is a set of activities that we perform on/with a system to determine
 * 	the quality of that system. 
 * 
 * 
 * Aside / Techniques for Testing
 * Boundary Value Analysis
 * Equivalence Partitioning
 * Decision Tables
 * State Transition Diagrams
 * 
 * 
 * 
 * 
 */







@SpringBootTest
class SpringDataIntroApplicationTests {
	
	@Autowired
	private PlanetRepo planetRepo;
	

	@Test
	void contextLoads() {
	}
	
	@Test
	public void testSave() {
		
		Planet p = new Planet();
		p.setId(-1); //Id value is generated for Planet, so our ID value here doesn't matter!
		p.setName("Venus");
		
		Planet savedPlanet = planetRepo.save(p);
		
		Optional<Planet> retrievedPlanet = planetRepo.findById(p.getId());
		
		assertThat(retrievedPlanet.isPresent());
		
		
		
	}

}
