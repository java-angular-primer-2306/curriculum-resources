package com.revature.presentation;

import com.fasterxml.jackson.core.type.TypeReference;
import com.revature.models.Planet;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@WebMvcTest(PlanetControllerRestTemplateTests.TestConfig.class)
public class PlanetControllerRestTemplateTests {

    @Configuration
    static class TestConfig {
        @Bean
        public RestTemplate restTemplate() {
            return new RestTemplate();
        }
    }

    @Autowired
    RestTemplate restTemplate;

    @Test
    public void shouldGetAllPlanets() {
        // N.B. your server needs to be running for this test to pass
        // RestTemplate is deprecated - but was mainly used to make HTTP requests to microservices or separate
        // services than you've included in this server
        // The preferred way is now to use HttpClient (https://www.baeldung.com/java-9-http-client)
        ResponseEntity<List<Planet>> responseEntity = restTemplate.exchange("http://localhost:8080/planet", HttpMethod.GET, null, new ParameterizedTypeReference<>(){});
        assertEquals(HttpStatusCode.valueOf(200), responseEntity.getStatusCode());
        List<Planet> planets = responseEntity.getBody();
        assertTrue((planets.size() > 0));
    }
}
