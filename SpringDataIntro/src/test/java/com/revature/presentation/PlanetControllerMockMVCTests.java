package com.revature.presentation;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.mockito.internal.matchers.GreaterThan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.BDDMockito.given;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.revature.models.Planet;
import com.revature.service.PlanetService;

// This annotation will only load the controller/presentation layer (anything annotated with @Controller)
// it is NOT loading services or repos


@WebMvcTest
public class PlanetControllerMockMVCTests {

//	// we can autowire any of our controller beans
//	@Autowired
//	private PlanetController planetController; 
//	
	// we must let Spring know about dependencies to inject
	@MockBean
	private PlanetService planetService; 
	
	@Autowired
	private MockMvc mockMvc;
	
	
	

	@Test
	public void givenData_whenGetAllPlanets_shouldReturnAllPlanets() throws Exception {
		
		List<Planet> mockPlanets = Arrays.asList(new Planet(1, "test planet"), new Planet(2, "test planet"));
		
		given(planetService.getAllPlanets()).willReturn(mockPlanets);
		
		// Build our Request
		RequestBuilder requestBuilder =   MockMvcRequestBuilders.get("/planet");
		
		// Perform the request (and optionally make some assertions with 'andExpect()' -> otherwise - parse object from response body and make assertions below
		MvcResult mvcResult = mockMvc.perform(requestBuilder)
			.andDo(print())
			.andExpect(status().isOk())
			.andExpect(jsonPath("$").isArray())
			.andExpect(jsonPath("$[0].id").value(1))
			.andExpect(jsonPath("$[0].name").value("test planet"))
			.andReturn();
		
		// assert
		
		// working directly with the response body
		String response = mvcResult.getResponse().getContentAsString();
		
		List<Planet> returnedPlanets = new ObjectMapper().readValue(response, List.class);
		
		assertEquals(2, returnedPlanets.size());
	}
	
	@Test
	public void givenData_whenGetById_shouldReturnPlanet() throws Exception {
		
		Planet mockPlanet = new Planet(10, "tenth planet");
		
		given(planetService.getPlanetById(anyInt())).willReturn(mockPlanet);
		
		// shortened version of above
		this.mockMvc.perform(get("/planet/1"))
			.andDo(print())
			.andExpect(status().isOk())
			.andExpect(content().contentType(MediaType.APPLICATION_JSON))
			.andExpect(jsonPath("$.id").value(10))
			.andReturn();
	}
	
	// POST Test
	@Test
	public void givenValidPlanet_whenSave_shouldReturnPlanetDetails() throws Exception {
		Planet planetToSave = new Planet(); 
		planetToSave.setName("new planet");
		
		Planet toReturn = new Planet(1, "new planet");
		given(planetService.savePlanet(planetToSave)).willReturn(toReturn);
		
		mockMvc.perform(post("/planet")
				.accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON)
				.content(toJson(planetToSave)))		// using helper method below to marshall into json
		.andDo(print())
		.andExpect(content().contentType(MediaType.APPLICATION_JSON))
		.andExpect(status().isCreated())
		.andExpect(jsonPath("$.id").isNotEmpty());
	}
	
	public String toJson(Object obj) {
		try {
			return new ObjectMapper().writeValueAsString(obj);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
}
