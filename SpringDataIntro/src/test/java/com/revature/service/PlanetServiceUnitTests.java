package com.revature.service;

import org.assertj.core.util.Arrays;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.BDDMockito.then;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import com.revature.models.Planet;
import com.revature.repo.PlanetRepo;

@ExtendWith(MockitoExtension.class) // required to properly inject the mock
public class PlanetServiceUnitTests {
	
	// When we're unit testing - we want to isolate the class under test
	// PlanetService depends on the PlanetRepo
		// we don't want any erroneous errors from the repo to fail our tests
		// we're focused on the logic of the service class
		// to simplify things - we're MOCKING the repo
	
	// For some interesting reads: look into State vs Behavior Validation
	// BDD - Behavior Driven Development: an extension of TDD - Test Driven Development
	// Given, When, and Then to describe how the feature should work - most often from an end-users perspective
	
	@InjectMocks
	private PlanetService planetService;
	
	@Mock
	private PlanetRepo planetRepo;
	
	// mock data
	List<Planet> mockPlanets = new ArrayList<>();
	
//	@BeforeEach
//	public void setUp() {
//		mockPlanets = Arrays.asList();
//		
//		// stub the behavior of the mock
//		when(planetRepo.findAll()).thenReturn(mockPlanets);
//		//given(planetRepo.findAll());
//	}
	
	@Test
	public void givenMockData_whenGetAllPlanets_thenReturnAllPlanets() {
		// Arrange / Given
		Planet p1 = new Planet(1, "mercury");
		Planet p2 = new Planet(2, "venus");
		Planet p3 = new Planet(3, "mars");
		
		mockPlanets.add(p1);
		mockPlanets.add(p2);
		mockPlanets.add(p3);
		
		when(planetRepo.findAll()).thenReturn(mockPlanets);
		
		// Act / When - actually invoking the method we're testing
		List<Planet> returnedPlanets = planetService.getAllPlanets();
		
		
		// Assert / Then
		verify(planetRepo, times(1)).findAll();// behavior
		
		then(planetRepo).should().findAll(); // behavior
		
		assertEquals(3, returnedPlanets.size()); // state
	}
	
	// Try writing a test for getPlanetById()
	

}
