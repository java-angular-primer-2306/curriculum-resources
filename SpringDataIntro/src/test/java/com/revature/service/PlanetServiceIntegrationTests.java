package com.revature.service;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;

import com.revature.models.Planet;

@SpringBootTest
@TestPropertySource(locations = "classpath:application-test.properties")
public class PlanetServiceIntegrationTests {

	// Integration Testing -> we are combining multiple components of our application together
	// and making sure they still work 
	// whereas with Unit Testing we did not combine the service with the repo. 
	
	@Autowired
	private PlanetService planetService;
	
	
	@Test
	public void getAllPlanetsIntegrationTest() {
		List<Planet> planets = planetService.getAllPlanets();
		
		assertTrue(planets.size() == 9); // planets.size() > 0
	}
	
	
}
