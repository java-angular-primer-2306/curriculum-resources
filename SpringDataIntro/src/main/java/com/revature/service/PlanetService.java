package com.revature.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.revature.models.Planet;
import com.revature.repo.PlanetRepo;

//A bean that is managed by Spring, declares this bean as a Service layer bean.

/*
 *  That will handle the business login, any request that comes from your controller class or resources that come from the 
 *  	repo layer will be handled by the service. 
 *  
 *  (Should be only Java code, does not care for any external domain specific resources (i.e. no JSONs, no XML resources, no ResultSet, etc ..) 
 */

@Service
public class PlanetService {
	
	private PlanetRepo planetRepo;
	
	
	public PlanetService(PlanetRepo planetRepo) {
		//more programmatic logic. 
		this.planetRepo = planetRepo;
	}


	public List<Planet> getAllPlanets() {
		return planetRepo.findAll();
	}


	public Planet getPlanetById(int id) {
		return planetRepo.findById(id).orElse(null);
	}


	public Planet savePlanet(Planet planet) {
		
		return planetRepo.save(planet);
	}
	
	


}
