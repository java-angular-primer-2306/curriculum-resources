package com.revature.presentation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.revature.models.Planet;
import com.revature.service.PlanetService;


//@Controller
@RestController
@RequestMapping("/planet")
public class PlanetController {
	
	private PlanetService planetService;
	
	@Autowired
	public PlanetController(PlanetService planetService) {
		this.planetService = planetService;
	}
	
	
	
	@GetMapping // requests coming to http://localhost:8080/planet
	public List<Planet> getAllPlanets() {
		return planetService.getAllPlanets();
	}

	@GetMapping(path = "/{id}", produces = "application/json") // requests coming to http://localhost:8080/planet/4
	public Planet getPlanetById(@PathVariable int id) {
		
		Planet planet = planetService.getPlanetById(id);
		if (planet == null) {
			System.out.println("planet doesn't exist");
			return null;
		}
		else {
			return planet;
		}
	}
	
	@PostMapping(consumes="application/json", produces = "application/json")
	@ResponseStatus(HttpStatus.CREATED)
	public Planet addPlanet(@RequestBody Planet planet) {
		return planetService.savePlanet(planet);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	

}
