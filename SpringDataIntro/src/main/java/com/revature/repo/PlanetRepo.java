package com.revature.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.revature.models.Planet;

/*
 * Differences between CrudRepositry and JpaRepositry 
 * 
 * What are their relationship?
 * 
 * What extra features do we get with Jpa? 
 * 
 */

public interface PlanetRepo extends JpaRepository<Planet, Integer>{

	
	Planet findByName(String name);
	
}
