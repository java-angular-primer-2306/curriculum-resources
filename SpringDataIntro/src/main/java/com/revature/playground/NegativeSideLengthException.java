package com.revature.playground;

public class NegativeSideLengthException extends RuntimeException {
	
	private String message; 
	
	public NegativeSideLengthException(String message) {
		super(message);
	}
	
}
