package com.revature.playground;

public class AreaCalculator {
	
	// Requirements: given a square's side length, this method should return
	// the area of the square
	public double  squareAreaBasic(double sideLength) {
		if (sideLength < 0) {
			System.out.println("Length must be greater than 0");
			return 0;
		}
		return Math.pow(sideLength, 2);
	}
	
	
	public double  squareAreaException(double sideLength) {
		if (sideLength < 0) {
			throw new NegativeSideLengthException("Side length must be greater than 0");
		}
		return Math.pow(sideLength, 2);
	}
	
	
	
	
	
	// given two sides of a rectangle, this method should return the area
	// of the rectangle.
	public double rectArea(double sideA, double sideB) {
		return sideA * sideB;
	}
	
	public static void main(String[] args) {
		AreaCalculator ac = new AreaCalculator();
		double area = ac.squareAreaBasic(5);
		System.out.println(area);
	}

}
