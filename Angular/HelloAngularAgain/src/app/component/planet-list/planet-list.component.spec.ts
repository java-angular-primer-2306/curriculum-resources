/// <j

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { PlanetListComponent } from './planet-list.component';
import { HttpClient } from '@angular/common/http';


fdescribe('PlanetListComponent', () => {
  let component: PlanetListComponent;
  let fixture: ComponentFixture<PlanetListComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [PlanetListComponent],
      providers: [HttpClient, {provide: HttpClient, useValue: HttpClientTestingModule}]
    });
    fixture = TestBed.createComponent(PlanetListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy(); // once we add Cypress to our project - VSCode will complain that this is not a method - because it's assuming expect() from Chai's library - instead of Jasmine's. Executing the tests will still work properly. 
  });
});
