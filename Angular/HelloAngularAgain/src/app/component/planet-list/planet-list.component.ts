import { Component } from '@angular/core';
import { Planet } from './planet';
import { PlanetServiceService } from '../../shared/planet-service.service';

@Component({
  selector: 'app-planet-list',
  templateUrl: './planet-list.component.html',
  styleUrls: ['./planet-list.component.css']
})
export class PlanetListComponent {


  //demo interpolation
  user: string = "Bob";
  age: number = 2;

  //demo structureal directives, ngFor and ngIf
  planets: Planet[] | Array<Planet>;

  //for image
  imageWidth=100;
  imageMargin=2;

  //for table
  showTable: boolean = true;
  toggleTable() {
    this.showTable = !this.showTable; //flip the value of showTable
  }


  //For filtering demo, to showcase 2 way binding 
  filteredPlanets: Planet[];
  inputFieldProperty: string = "";

  get inputField(){
    return this.inputFieldProperty;
  }

  set inputField(userInput: string){
    this.inputFieldProperty = userInput;

    //If the inputfield is not empty we will filter the planet, if it is empty then return the planet list
    this.filteredPlanets = (this.inputFieldProperty)?this.performFilter(this.inputField):this.planets
  }

  performFilter(filterValue: string): Planet[] {
    
    return this.planets.filter(
      (planet:Planet) => planet.name.toLowerCase().indexOf(filterValue.toLowerCase())!= -1
    )
  }
  

  constructor(private planetService: PlanetServiceService){
    
    this.planets = []; //1

    this.planetService.capturePlanets().subscribe( //2 

      (data)=> {

        console.log("I'm getting data!")
        console.log(data);
        this.planets = data;

        this.filteredPlanets = this.planets;
      }

    );

    console.log(this.planets); //3

    this.filteredPlanets = this.planets; //4
  }

}
