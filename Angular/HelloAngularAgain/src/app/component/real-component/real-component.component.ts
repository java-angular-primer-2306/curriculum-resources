import { Component } from '@angular/core';

@Component({
  selector: 'real-component',
  templateUrl: './real-component.component.html',
  styleUrls: ['./real-component.component.css','./even-more-styling.css']
})
export class RealComponentComponent {

}
