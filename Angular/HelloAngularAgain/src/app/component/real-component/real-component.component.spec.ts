import { ComponentFixture, TestBed } from "@angular/core/testing";
import { RealComponentComponent } from "./real-component.component";


fdescribe('RealComponentTests', ()=>{

    let component: RealComponentComponent;
    let fixture: ComponentFixture<RealComponentComponent>;


    beforeEach(()=>{

        TestBed.configureTestingModule({
            declarations: [RealComponentComponent]
        })
            .compileComponents();

        fixture = TestBed.createComponent(RealComponentComponent);
        component = fixture.componentInstance;

    });


    it('should load component', ()=>{
        expect(component).toBeTruthy()
    });
});