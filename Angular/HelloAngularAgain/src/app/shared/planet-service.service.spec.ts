import { TestBed } from '@angular/core/testing';

import { PlanetServiceService } from './planet-service.service';
import { HttpClient } from '@angular/common/http';
import { Planet } from '../component/planet-list/planet';
import { of } from 'rxjs';

fdescribe('PlanetService with Spy', () => {
  let planetService: PlanetServiceService;
  let httpClientSpy: jasmine.SpyObj<HttpClient>; // sometimes you might see a spy with 'any' datatype

  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get']);
    planetService = new PlanetServiceService(httpClientSpy);
  });

// done: DoneFn will allow us to perform asynchronous tests 
// we call done() when we've finished doing async operations
  it('should return expected planets', (done: DoneFn) =>{

    const planetData: Planet[] = [
        {
            name: 'earth 2.0',
            image: 'fakeurl', 
            livibility: 10
        }
    ];
    // like in Mockito when(mock.get).thenReturn()
    httpClientSpy.get.and.returnValue(of(planetData));

    planetService.capturePlanets().subscribe({
        next: planets => {
            expect(planets).toEqual(planetData);
            done();
        },
        error: done.fail
    });

    expect(httpClientSpy.get.calls.count())
        .toBe(1);
  });

  it('should be created', () => {
    expect(planetService).toBeTruthy();
  });
});
