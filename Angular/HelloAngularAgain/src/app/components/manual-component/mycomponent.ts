import { Component } from "@angular/core";

@Component({
    selector: "myComponent",
    template: "<h1> Hello there!</h1>", //Require a HTML file
    styleUrls: [] // Require a CSS file 
})
export class myComponent{ //Also for testing, we would typically have a spec.ts file 

}