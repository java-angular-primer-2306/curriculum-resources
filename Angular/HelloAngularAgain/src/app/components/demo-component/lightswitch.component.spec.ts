import { LightswitchComponent } from "./lightswitch.component";


describe('LightswitchComponent Tests', () =>{

    it(' clicked() should toggle on/off', ()=> {
        const comp = new LightswitchComponent();
        expect(comp.isOn).toBe(false);
        comp.clicked();
        expect(comp.isOn).toBe(true);
        comp.clicked();
        expect(comp.isOn).toBe(false);
    });

    it('should display message', () =>{
        const comp = new LightswitchComponent();
        expect(comp.message).toBe('The light is Off');
        comp.clicked();
        expect(comp.message).toBe('The light is On')
    });

});