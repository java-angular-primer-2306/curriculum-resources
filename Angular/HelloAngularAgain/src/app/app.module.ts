import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { myComponent } from './components/manual-component/mycomponent';
import { RealComponentComponent } from './component/real-component/real-component.component';
import { PlanetListComponent } from './component/planet-list/planet-list.component';

import {FormsModule} from '@angular/forms'
import { HttpClientModule } from '@angular/common/http'

import { PlanetServiceService } from './shared/planet-service.service';
import { LightswitchComponent } from './components/demo-component/lightswitch.component';



@NgModule({
  declarations: [
    AppComponent,
    myComponent,
    RealComponentComponent,
    PlanetListComponent,
    LightswitchComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [PlanetServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
