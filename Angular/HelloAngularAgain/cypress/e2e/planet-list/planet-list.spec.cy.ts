describe("PlanetList E2E Tests", ()=>{

    beforeEach(()=>{
        cy.visit('/')
    });


    it('should display at least one earth', ()=>{

        cy.get('table').find('tr').should('have.length.at.least', 1);

    });

    it('should toggle table', ()=>{

        cy.get('button').first().as('toggleTableButton').click();

        cy.get('.table-responsive').should('not.have.descendants', 'table');

        cy.get('@toggleTableButton').click();

        cy.get('.table-responsive').should('have.descendants', 'table' );
    });

    it('should toggle lightswitch', ()=> {

        cy.get('button').last().as('lightSwitchButton').click();

        cy.get('@lightSwitchButton').next().as('onOffMessage').should('have.text', 'The light is On');

        cy.get('@lightSwitchButton').click();

        cy.get('@onOffMessage').should('have.text', 'The light is Off');
    });



});