describe('My First Test', () => {
  it('Visits the initial project page', () => {
    cy.visit('/') // visit the baseUrl "http://localhost:4200"
    cy.contains('Bob')
  })

  //cypress uses CSS Selectors to identify elements on the page
  it('should show earth images', () =>{
    cy.visit('/')

    cy.get('img').should('have.length', 9)
  })
})
