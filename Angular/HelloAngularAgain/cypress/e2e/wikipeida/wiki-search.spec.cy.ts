describe('wiki search bar works', ()=>{

    beforeEach(()=>{
        cy.visit("https://wikipedia.org")
    });

    it('should find mario page', ()=>{
        cy.get('input[id="searchInput"]').type('mario')
        cy.get('button').first().click()

        cy.get('title').should('have.text', 'Mario - Wikipedia')
    });

    it('should find mario page', ()=>{
        cy.get('input[id="searchInput"]').type('luigi')
        cy.get('button').first().click()

        cy.get('title').should('have.text', 'Luigi - Wikipedia')
    });

});