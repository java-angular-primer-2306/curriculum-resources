
describe('wiki lang links work', ()=> {

    beforeEach(()=>{
        cy.visit('https://wikipedia.org')
    });

    it('english link works', ()=>{
        cy.get('a[id="js-link-box-en"]').click()
        cy.get('title').should('have.text', 'Wikipedia, the free encyclopedia')
    });

    it('german link works', ()=>{
        cy.get('a[id="js-link-box-de"]').click()
        cy.get('title').should('have.text', 'Wikipedia – Die freie Enzyklopädie')
    });

    it('spanish link works', ()=>{
        cy.get('a[id="js-link-box-es"]').click()
        cy.get('title').should('have.text', 'Wikipedia, la enciclopedia libre')
    });

    it('french link works', ()=>{
        cy.get('a[id="js-link-box-fr"]').click()
        cy.get('title').should('have.text', "Wikipédia, l'encyclopédie libre")
    });

});