/**
 * 
 */
/**
 * @author benjaminarayathel
 *
 *	This is the Java Platform Module System (JPMS) - it doesn't change how Java files are packaged into JAR files. Instead it
 *	adds additional descriptors to JARs so that we can go ahead and organize our code better. (Designed for Large scale apps) 
 *
 *	> Make it easier for developers to organized large apps and libraries 
 *  > Improves the structure and security of the platform 
 *  > Improves App performance. 
 *
 *
 */
module Java17AndJava11 {
		exports com.revature.example;
		exports com.revature.another.packag;
}