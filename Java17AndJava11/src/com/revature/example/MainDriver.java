package com.revature.example;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Stream;


public class MainDriver {
	
	/**
	 * 
	 * Stream API: 
	 * 	Feature that was introduced in Java 8. It provides a functional programming approach to collections. 
	 * 
	 * 	> Simplified and consie code. 
	 *  > Enhanced performances. 
	 *  > Declarative programming style. 
	 * 
	 * 	What is Functinoal Programming?
	 * 		Function oriented programming, where functions drive the code. This is contrasted with OOP code, where
	 * 			functions are treated as second class entitties to objects and classes. 
	 * 
	 * 
	 * Stream API Operations: 
	 * 
	 * https://www.geeksforgeeks.org/intermediate-methods-of-stream-in-java/
	 * Intermediate Operations: 
	 * 		.filter(Predicate);
	 * 		.sorted();
	 * 		.distinct();
	 * 		.skip(Integer n)
	 * 
	 * 
	 * Terminal Operations: 
	 * 		.forEach()
	 * 		.count()
	 * 		.min() & .max()
	 * 		.toList()
	 * 
	 * 
	 * Java 11 is a LTS version of the JDK. Technically, Oracle no longer provides a JDK for free, instead we start using 
	 * 	JDK version supported by other companies. 
	 * 
	 * With Java 11, we get a few new features. 
	 * 
	 *  > New String Methods 
	 *  	isBlank, lines, strip,stripLeaing, stripTrailing and Repeat.
	 *  		(Reducing boiler plate code)
	 *  
	 *  > Collection to an Array 
	 *  	(reduce boiler plate code)
	 *  
	 *  > Not Predicates Method 
	 *  	(reduce boiler plate code) 
	 *  
	 *  > Local Variable Syntax for Lambda 
	 *  	(introduces var)
	 * 
	 * 
	 */

	public static void main(String[] args) {
		
		
		// STREAM API Examples------------------------------------------------------------------------
		Collection<String> aCollection = initializeString();
		
		streamMethodsCountingDistinct(aCollection);
		
		
		//This method is throwing an error, see if you can fix it
//		streamMethodsFilteringBlanks(aCollection);
		
		streamMethodsDoABunchOfEverything(aCollection);
		
		//New String Methods--------------------------------------------------------------------------
		newStringMethodsfromJava11("               Example String over here !                   ");
		
		
		//New Collection to Array Methods-------------------------------------------------------------
		collectionToAnArray(aCollection);

		
	}

	
	//Collection interface contains a new default toArray method which makes it easier to convert a Collection to Array
	public static String[] collectionToAnArray(Collection<String> stringList){
		
		//The old way: 
		Object[] oArray = stringList.toArray(); //List<String> -> Object[]
		String[] sArray = (String[]) oArray;
		
		//Java 11 better way:
		String[] s2Array = stringList.toArray(String[]::new); //List<String> -> String[]
		
		
		return s2Array;
	}
	
	public static void newStringMethodsfromJava11(String s) {
		
		System.out.println("This string is " + (s.isBlank()?"blank":"not blank"));
		
		System.out.println("We can remove the spacing between the trailing and leading letters by using the .strip() method: e.g." + s + " -> " + s.strip());
		
		System.out.println("stripLeading() will removing the leading blank space: e.g. " + s + " -> " + s.stripLeading()); 
		
		System.out.println("stripTrailing() will removing the trailing blank space: e.g. " + s + " -> " + s.stripTrailing()); 
		
		System.out.println("repeat(Integer n) will repeat the string n amounts of times");
		
		
	};
	
	public static void streamMethodsFilteringBlanks(Collection<String> stringCollection) {

		Stream<String> streamOfStrings = stringCollection.stream();
		
		streamOfStrings.filter(null).forEach( s -> System.out.println(s));
		
		
	};
	
	public static long streamMethodsCountingDistinct(Collection<String> stringCollection) {
		
		Stream<String> streamOfStrings = stringCollection.stream();
		
		long totalDistinctObjects = streamOfStrings.distinct().count();
		
		return totalDistinctObjects;
		
	}
	
	//Removing "Apple" string from stream, removing blank strings (using Predicates' static method not() ), sorting it in natural order, and then converting it back to a list
	public static List<String> streamMethodsDoABunchOfEverything(Collection<String> stringCollection) {
		
		Stream<String> streamOfStrings = stringCollection.stream();
		
		List<String> streamedList = streamOfStrings.
		filter(Predicate.not(s -> s.equalsIgnoreCase("Apple"))).
		filter(Predicate.not(String::isBlank)).
		sorted().
		toList();
		
		return streamedList;
		
	}
	
	public static Collection<String> initializeString(){
		
		Collection<String> aCollection = new ArrayList<>();
		
		aCollection.add("Apple");
		aCollection.add("Kiwi");
		aCollection.add("Kiwi");
		aCollection.add("Kiwi");
		aCollection.add("Kiwi");
		aCollection.add("Kiwi");
		aCollection.add("Tomatoe");
		aCollection.add("Pear");
		aCollection.add("");
		aCollection.add("");
		aCollection.add("");
		aCollection.add("");
		aCollection.add("");
		
		return aCollection;
		
	}

	
}
