import { Calculator } from '../calculator.js'

fdescribe('Calculator Ops tests', ()=>{
    // all of our tests go inside this function

    // each test has it's own separate block of code
    it('should add two positive integers', () => {

        const calc = new Calculator(); 
        const result = calc.add(4, 5);

        expect(result).toBe(9);

    });

    it('should add one negative and one positive number', () => {
        const calc = new Calculator(); 
        const result = calc.add(-4, 5);

        expect(result).toBe(1);
    });



});

describe('sanity test', () =>{
    it('truthy', ()=> {

        expect(true).toBeTruthy();
    })
    
});