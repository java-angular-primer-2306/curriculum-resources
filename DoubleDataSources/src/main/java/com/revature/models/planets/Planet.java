package com.revature.models.planets;

import java.util.Objects;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

/**
 * 
 * JPA Annotation (Javax/Jarkata Persistence API) 
 * 
 * Is a standardized api that deals with mapping Java objects to records in tables. 
 * It is not a library or app, instead it defines a set of rules for all ORM (Object relatational mapping) libraries to follow. 
 * 
 * We choose JPA over Hibernate annotations because it makes it more modular. 
 *
 */

@Entity
@Table(name = "planet_table")
public class Planet {
	
	@Id
	@Column(name = "planet_id")
	private int id;
	
	@Column(name="planet_name", unique = true)
	private String name;
	
	@Column(name="planet_mass", nullable = false)
	private double mass;
	
	@Column(name="planet_rings")
	private boolean hasRings;

	public Planet(int id, String name, double mass) {
		super();
		this.id = id;
		this.name = name;
		this.mass = mass;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getMass() {
		return mass;
	}

	public void setMass(double mass) {
		this.mass = mass;
	}

	public boolean isHasRings() {
		return hasRings;
	}

	public void setHasRings(boolean hasRings) {
		this.hasRings = hasRings;
	}

	@Override
	public String toString() {
		return "Planet [id=" + id + ", name=" + name + ", mass=" + mass + ", hasRings=" + hasRings + "]";
	}

	@Override
	public int hashCode() {
		return Objects.hash(hasRings, id, mass, name);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Planet other = (Planet) obj;
		return hasRings == other.hasRings && id == other.id
				&& Double.doubleToLongBits(mass) == Double.doubleToLongBits(other.mass)
				&& Objects.equals(name, other.name);
	}

	public Planet(int id, String name, double mass, boolean hasRings) {
		super();
		this.id = id;
		this.name = name;
		this.mass = mass;
		this.hasRings = hasRings;
	}

	public Planet() {
		super();
		// TODO Auto-generated constructor stub
	}

	
	
	
	
	
	
	

}
