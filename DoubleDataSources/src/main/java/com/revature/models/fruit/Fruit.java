package com.revature.models.fruit;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table
public class Fruit {
	
	@Id
	@Column(name="fruit_id")
	private int id;
	
	@Column
	private String name;
	
	@Column
	private boolean isRed;
	
	

}
