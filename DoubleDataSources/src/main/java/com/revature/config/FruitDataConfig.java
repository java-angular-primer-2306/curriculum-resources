package com.revature.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import jakarta.persistence.EntityManagerFactory;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
		entityManagerFactoryRef="fruitEntityManagerFactory",
		transactionManagerRef= "fruitTransactionManager",
		basePackages= "com.revature.repo.fruit"
		
		)
public class FruitDataConfig {

	
	@Bean(name="fruitProperties")
	@ConfigurationProperties("spring.datasource.two")
	public DataSourceProperties dataSourceProperties() {
		
		return new DataSourceProperties();
	}
	
	@Bean(name="fruitDataSource")
	public DataSource dataSource(@Qualifier("fruitProperties") DataSourceProperties properties) {
		return properties.initializeDataSourceBuilder().build();
	}
	
	//Entity Manager - resposible for managing our entities i.e. Planet.class
	@Bean(name = "fruitEntityManagerFactory")
	public LocalContainerEntityManagerFactoryBean entityManagerFactoryBean(
			EntityManagerFactoryBuilder builder,
			@Qualifier("fruitDataSource")DataSource dataSource
			) {
		
		return builder.dataSource(dataSource)
				.packages("com.revature.models.fruit")
				.persistenceUnit("fruit").build();
		
	}
	
	//Transaction Manager is dependent on EntityManager and Data Source
	@Bean(name = "fruitTransactionManager")
	@ConfigurationProperties("spring.jpa")
	public PlatformTransactionManager transactionManager(
			@Qualifier("fruitEntityManagerFactory") EntityManagerFactory entityManagerFactory) {
		
		JpaTransactionManager jpaTM =  new JpaTransactionManager(entityManagerFactory);
		
		return jpaTM;
	}
	
	
}
