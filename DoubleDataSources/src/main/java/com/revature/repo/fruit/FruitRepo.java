package com.revature.repo.fruit;

import org.springframework.data.jpa.repository.JpaRepository;

import com.revature.models.fruit.Fruit;

public interface FruitRepo extends JpaRepository<Fruit, Integer>{

}
