# RevPay Web Application (Due on 27th June)

## Description

   The RevPay project aims to develop a secure, user-friendly, and versatile financial application for personal users. The core functionalities for personal accounts include sending or requesting money, adding credit and debit cards, and view the transaction history.

### RevPay requirements

    1. As a person, I should be able to: 
    2. Register myself and create an account. 
    3. Login to my account. 
    4. Send money by username, email, or phone number. 
    5. Request money by username, email, or phone number. 

   The requirements for the application itself is left open ended to let you decide how to best implement the features. You are also allowed to create your own project (with a similar level of technical difficulty) but you must first reach out to your manager for approval.

## Purpose

   We want to see that you can build an full stack application  using the technologies that have been taught so far as well incoporate techniques and technologies you will learn in this primer. In addition to that, we want to see that you are able to deploy the application onto a remote server in a containerized environment.

## Technical Requirements

1. Build a CI pipeline that builds and pushes your Spring Boot image to a remote repository
2. Jasmine/Karma testing for Angular.
3. E2E testing with Cypress
4. Integration testing in Spring, using MockMVC.
5. Unit testing with JUnit and Mockito

NOTE: The primary focus of the technical requirements lies in the utilization of the technologies covered in the primer. However, it is still imperative to develop a polished and proficient application that fulfills the criteria of a fundamental app.

## Technologies

1. Spring Boot
2. Spring Data
3. H2
4. Docker  
5. Angular
6. Any 3rd Party Plugins as required
